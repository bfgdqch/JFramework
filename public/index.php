<?php
/**
 * Created by PhpStorm.
 * User: Qch
 * Date: 2016/8/13
 * Time: 17:03
 *
 * 入口文件
 *    定义路径常量
 */
/**
 * 引入composer
 */
require "../vendor/autoload.php";
/**
 * 开启跨域
 */
// define('OPEN_CORS', true)

/**
 * 网站起始目录
 */
const __WWW_DIR__ = __DIR__;

/**
 * 运行时目录
 */
define('__RUNTIME__', dirname(__WWW_DIR__));

/**
 * 定义时区
 */
date_default_timezone_set('PRC');

/**
 * 加载系统库文件
 */
include_once("../src/Aoe/Core/index.php");