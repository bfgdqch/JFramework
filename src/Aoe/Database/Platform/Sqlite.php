<?php

namespace Aoe\Database\Platform;

use Aoe\Emulator\Ifc\Type;
use Aoe\Database\Platform;
use PDO;

class Sqlite extends Platform
{
    public function useIdSql(bool $uuid): string
    {
        return $uuid ?
            '   id INTEGER PRIMARY KEY NOT NULL,' :
            '   id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,';
    }
    
    public function useStatusSql(): string
    {
        $str = Type::STATUS;
        return "  $str BIT DEFAULT 1 NOT NULL";
    }
    
    public function useAfterSql(array $table, bool $simple): string
    {
        return ');';
    }
    
    public function useCommentSql(array $obj): string
    {
        return '';
    }
    
    public function useDefaultValueSql($value): string
    {
        return "DEFAULT($value)";
    }
    
    public function useIntString(): string
    {
        return 'INTEGER';
    }
    
    public function useBitString(): string
    {
        return 'TINYINT';
    }
    
    public function useUnsigned(): string
    {
        return '';
    }
    
    public function canGroup(): bool
    {
        return false;
    }
    
    protected function make_dsn(array $config): string
    {
        $file = __DATA_DIR__ . DIRECTORY_SEPARATOR . $config['name'];
        return "sqlite:$file";
    }
    
    protected function on_connected(Pdo $pdo, array $params): void {}
    
    
    public function mapCbl(string $column, mixed $param): string
    {
        $v = Platform::int_2_birth($param);
        return "CAST(SUBSTRING($column, 7, 6) AS INT) < $v";
    }
    
    public function mapCbg(string $column, mixed $param): string
    {
        $v = Platform::int_2_birth($param);
        return "CAST(SUBSTRING($column, 7, 6) AS INT) > $v";
    }
}