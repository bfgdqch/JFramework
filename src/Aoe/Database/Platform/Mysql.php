<?php
/**
 * Created by PhpStorm.
 * User: Qch
 * Date: 2017/3/24
 * Time: 16:44
 */

namespace Aoe\Database\Platform;

use Aoe\Database\Platform;

class Mysql extends Platform
{
    protected function make_dsn(array $config): string
    {
        $ps = array_map(fn ($key) => "$key=$config[$key];", array_filter(
            ['host', 'port', 'unix_socket', 'charset'],
            fn ($key) => !empty($config[$key])
        ));
        $ps[] = "dbname={$config['name']};";
        
        return 'mysql:' . join($ps);
    }
}