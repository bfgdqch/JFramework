<?php


namespace Aoe\Database;

use PDO;

/**
 * # 数据库操作数
 */
readonly class Sprite
{
    /**
     * ##
     *
     * @param string|null $operator 指令
     * @param mixed  $param       参数
     * @param int    $binding     参数类型
     * @param string $column      数据表列名称
     */
    public function __construct(
        /**
         * @var string|null 操作符
         */
        private string|null $operator,
        /**
         * @var mixed 值
         */
        private mixed  $param,
        /**
         * @var int PDO值类型
         */
        private int    $binding   = 0,
        /**
         * @var string 列名称
         */
        private string $column = '',
    ) {}
    
    public function getOperator(): string
    {
        return $this->operator;
    }
    
    public function getParam(): mixed
    {
        return $this->param;
    }
    
    public function getBinding(): int
    {
        if (null === $this->param) return PDO::PARAM_NULL;
        return $this->binding;
    }
    
    public function getColumn(): string
    {
        return $this->column;
    }
    public function __toString() : string
    {
        return $this->column . ' => ' . $this->param;
    }


}