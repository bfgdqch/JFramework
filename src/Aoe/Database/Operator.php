<?php


namespace Aoe\Database;

/**
 * # 数据操作符
 */
abstract class Operator
{
    const string LIKE_OPERATOR       = 'LIKE';
    const string NOT_LIKE_OPERATOR   = 'NOT-LIKE';
    const string IN_OPERATOR         = 'IN';
    const string NOT_IN_OPERATOR     = 'NOT-IN';
    const string NOT_NULL_OPERATOR   = 'NOT-NULL';
    const string IS_NULL_OPERATOR    = 'IS-NULL';
    const string INT_EMPTY_OPERATOR  = 'NOT-RELATION';
    const string RELATION_OPERATOR   = 'IS-RELATION';
    const string STR_EMPTY_OPERATOR  = 'NOT-CONTENT';
    const string CONTENT_OPERATOR    = 'IS-CONTENT';
    const string CARD_BIRTH_LESS     = 'CARD-BIRTH-LESS';
    const string CARD_BIRTH_GREAT    = 'CARD-BIRTH-GREAT';
    const string BETWEEN_OPERATOR    = 'BETWEEN';
    const string NOW_OPERATOR        = 'NOW';
    const string SET_NULL_OPERATOR   = 'SET-NULL';
    const string IP_OPERATOR         = 'CLIENT';
    const string RAND_OPERATOR       = 'RAND';
    const string REPLACE_OPERATOR    = 'REPLACE';
    const string AS_OPERATOR         = 'AS';
    const string EXPRESSION_OPERATOR = 'EXPRESSION';
    
    public static function isArrayCompare(string $operator): bool
    {
        return in_array($operator, [
            self::IN_OPERATOR, self::NOT_IN_OPERATOR, self::BETWEEN_OPERATOR
        ]);
    }
    
    /** @noinspection PhpUnusedParameterInspection */
    public static function isEmptyCompare(string $operator): bool
    {
        return false;
    }
    
    public static function isEmptyCalculator(string $operator): bool
    {
        return in_array($operator, [
            self::NOW_OPERATOR, self::IP_OPERATOR, self::SET_NULL_OPERATOR, self::RAND_OPERATOR
        ]);
    }
    
    public static function isArrayCalculator(string $operator): bool
    {
        return $operator === self::REPLACE_OPERATOR;
    }
    
    /**
     * ## 表达式
     *
     *  > 校验参数，并返回修正好的sql，校验失败抛出异常
     *
     * @param mixed  $value 该参数需要自主验证
     * @param string $name
     * @param ?array $input 全部参数
     *
     * @return string|false
     */
    abstract public function getStatement(mixed $value, string $name, ?array $input): string | false;
}