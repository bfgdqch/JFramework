<?php

namespace Aoe\Util\Manager;

use Exception;

/**
 * # 集合键可以是数组
 *
 * @template T extend object
 * @extends NamedCollection<T>
 */
abstract class ArrayedCollection extends NamedCollection
{
    public function has(string | array $name): bool
    {
        if (!is_array($name)) return parent::has($name);
        return isset($name['name']) && is_scalar($name['name']) && parent::has($name['name']);
    }

    /**
     * @param string | array $name
     * @return null|T
     * @throws Exception
     */
    public function get(string | array $name): ?object
    {
        if (!is_array($name)) return parent::get($name);

        if(!isset($name['name']) || !is_scalar($name['name'])) return null;

        return parent::get($name['name']);
    }
}