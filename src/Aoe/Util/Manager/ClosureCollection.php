<?php

namespace Aoe\Util\Manager;

use Closure;

/**
 * # 闭包生成对象管理器
 *
 * @template T extend object
 * @extends Collection<T>
 */
class ClosureCollection extends Collection
{
    /**
     * ##
     *
     * * 通过creator实现子对象创建自定义
     *
     * @param closure($name, $arguments): T $creator
     *
     */
    public function __construct(protected Closure $creator) {}
    
    protected function create_object(mixed $name): ?object
    {
        return ($this->creator)($name);
    }
}