<?php
/**
 * Created by PhpStorm.
 * User: Qch
 * Date: 2016/9/8
 * Time: 16:16
 */

namespace Aoe\Util;


class File
{
    /**
     * 读取文件信息
     *
     * @access public
     *
     * @param string          $index 文件名
     * @param null|bool|array $vars  读取内容、数组、模板
     *
     * @return null|array|string content
     */
    public static function get(string $index, array | null | bool $vars = null): bool | array | string
    {
        if (!is_file($index)) return false;
        
        if ($vars === null) return self::getString($index);
        
        if ($vars === true) return self::getArray($index);
        
        if (is_array($vars)) return self::getTemplate($index, $vars);
        
        return false;
    }
    
    /**
     * 读取文件内容字符串
     *
     * @param string $file 文件名
     *
     * @return string|bool
     */
    public static function getString(string $file): string | bool
    {
        if (!is_file($file)) return false;
        
        return file_get_contents($file);
    }
    
    /**
     * 获取文件定义的数组
     *
     * @param string $filename 文件名
     *
     * @return array|false
     */
    public static function getArray(string $filename): array | false
    {
        if (!is_file($filename)) return false;
        
        $v = require($filename);
        return is_array($v) ? $v : false;
    }
    
    public static function getTemplate(string $file, array $vars): string | bool
    {
        if (!is_file($file)) return false;
        
        $__ = $file;
        unset($vars['__']);// 防止意外重写了文件名
        extract($vars);
        ob_start();
        include $__;
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }
    
    /**
     * 文件写入
     *
     * @param string       $file
     * @param array|string $value
     *
     * @return false | int
     */
    public static function add(string $file, array | string $value): false | int
    {
        
        if (empty($file)) return false;
        
        $dir = dirname($file);
        if (!is_dir($dir) && !mkdir($dir, 0777, true)) return false;
        
        if (is_array($value)) $value = '<?php return ' . var_export($value, true) . ';';
        return file_put_contents($file, $value);
    }
    
    /**
     * 文件是否存在
     *
     * @access public
     *
     * @param string $filename 文件名
     *
     * @return boolean
     */
    public static function has(string $filename): bool
    {
        return is_file($filename);
    }
    
    /**
     * 文件删除
     *
     * @access public
     *
     * @param string $key 文件名
     *
     * @return boolean
     */
    public static function remove(string $key): bool
    {
        return is_file($key) && unlink($key);
    }
    
    /**
     * @param string $key    文件名
     * @param ?int   $expire 过期时间
     *
     * @return bool
     * @noinspection PhpUnused
     */
    public static function isExpire(string $key, ?int $expire = null): bool
    {
        return is_file($key) and (!$expire or filemtime($key) > $expire);
    }
}