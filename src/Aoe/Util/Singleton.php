<?php

namespace Aoe\Util;

Trait Singleton
{
    private static array $instances = [];
    
    protected function __construct() {}
    
    public static function getInstance() {
        $className = static::class;
        if (!isset(self::$instances[$className])) {
            self::$instances[$className] = new static();
        }
        return self::$instances[$className];
    }
}