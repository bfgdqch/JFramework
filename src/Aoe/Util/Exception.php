<?php


namespace Aoe\Util;

use Closure;
use Throwable;

/**
 * # 实现错误信息本地化的异常类
 */
class Exception extends \Exception
{
    /**
     * @var ?Closure 错误信息本地化翻译器
     */
    protected static ?Closure $translator = null;
    
    public function __construct($message = "", $code = 0, ?Throwable $previous = null)
    {
        $context             = is_int($code) ? [] : $code;
        $context['__file__'] = $this->getFile();
        $context['__line__'] = $this->getLine();
        if (self::$translator) $message = (self::$translator)($message, $context);
        parent::__construct($message, 0, $previous);
    }
    
    public static function setTranslator(Closure $translator): void
    {
        self::$translator = $translator;
    }
}