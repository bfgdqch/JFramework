<?php

namespace Aoe\Util\Logger;

use Aoe\Util\Swap\Rom;

/**
 * # 日志记录
 *
 * @property string name
 * @property int level
 * @property string message
 * @property int line
 * @property string file
 */
class Record extends Rom
{
    const string NAME    = 'name';
    const string LEVEL   = 'level';
    const string MESSAGE = 'message';
    const string FILE    = 'file';
    const string LINE    = 'line';
}