<?php

namespace Aoe\Util\Logger;

/**
 * # 命名记录器
 */
readonly class NameRecorder
{
    /**
     * ##
     *
     * @param Logger $logger 日志对象
     * @param string $name 记录器名称
     */
    public function __construct(
        private Logger $logger,
        private string $name,
    ){}

    /**
     * ## 记录错误信息
     *
     * @param string $message 错误信息
     * @param array $context 参数
     * @param int $back 发生地回退层级
     *
     * @return string
     */
    public function Error(string $message, array $context = [], int $back = 1): string
    {
        return $this->Log($message, $context, Logger::ERROR, $back + 1);
    }

    /**
     * ## 记录日志信息
     *
     * @param string $message 日志信息
     * @param array $context 参数
     * @param int $level 信息等级
     * @param int $back 调用层次
     *
     * @return string
     */
    public function Log(string $message, array $context = [], int $level = Logger::DEBUG, int $back = 1): string
    {
        return $this->logger->insert($this->name, $level, $message, $context, $back);
    }

    /**
     * @return Record[]
     * @noinspection PhpUnused
     */
    public function getRecords(int $level = Logger::ALL): array
    {
        return $level ? array_filter(
            $this->logger->getRecords(),
            fn(Record $r) => $r->name === $this->name && $r->level & $level)
            : array_filter($this->logger->getRecords(), fn(Record $r) => $r->name === $this->name);
    }
}