<?php

namespace Aoe\Util\Logger;

/**
 * 名称等级记录器
 */
readonly class NameLevelRecorder
{
    public function __construct(
        private NameRecorder $recorder,
        private int $level,
    ) {}

    /**
     * ## 记录错误信息
     *
     * @param string $message 错误信息
     * @param array  $context 参数
     * @param int    $back    发生地回退层级
     *
     * @return string
     */
    public function Error(string $message, array $context = [], int $back = 1): string
    {
        return $this->recorder->Error($message, $context, $back + 1);
    }

    /**
     * ## 记录日志信息
     *
     * @param string $message 日志信息
     * @param array  $context 参数
     * @param int    $back    调用层次
     *
     * @return string
     */
    public function Log(string $message, array $context = [], int $back = 1): string
    {
        return $this->recorder->Log($message, $context, $this->level, $back + 1);
    }

    /**
     * @return Record[]
     * @noinspection PhpUnused
     */
    public function getRecords(): array
    {
        return array_filter($this->recorder->getRecords(), fn(Record $r) => $r->level | $this->level);
    }
}