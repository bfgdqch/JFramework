<?php

namespace Aoe\Util;

use Exception;

/**
 * # 模板编译引擎
 */
interface Engine
{
    /**
     * ## 编译模板
     *
     * @param string $file 模板文件
     *
     * @return string 编译结果
     *
     * @throws Exception
     */
    public function compile(string $file): string;
    
    /**
     * ## 渲染模板
     *
     * @param string $content
     * @param        $params
     *
     * @return string
     */
    public function display(string $content, $params): string;
}