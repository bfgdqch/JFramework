<?php

namespace Aoe\Util;

/**
 * # 中间件流水线，实现洋葱模式
 */
class Pipeline
{
    /**
     * ## 启动洋葱型流水线
     *
     * @param callable[] $middlewares 中间件数组
     * @param callable   $first       内芯函数
     * @param mixed      ...$context  参数
     *
     * @return mixed
     */
    public static function start(array $middlewares, callable $first, ...$context): mixed
    {
        $func = array_reduce(
            $middlewares,
            function ($next, $item) {
                // 闭包直接返回
                if (is_callable($item)) return $item($next);
                // 跳过
                return $next;
            },
            $first,
        );
        
        return $func(...$context);
    }
}