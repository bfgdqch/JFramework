<?php


namespace Aoe\Util\Bus;


/**
 *
 * # 缓存接口
 */
interface ICache extends ISolid
{
    /**
     * @param string   $key
     * @param bool|int $before 过期时间点
     *
     * @return bool
     */
    public function has(string $key, bool | int $before = false): bool;
    
    public function clear();
}