<?php

namespace Aoe\Util\Bus;

use Exception;

/**
 * # 存取器
 */
class Accessor
{
    public function __construct(protected ISolid $store, protected string $key) {}
    
    /**
     * @throws Exception
     */
    public function load(): mixed
    {
        return $this->store->get($this->key);
    }
    
    /**
     * @throws Exception
     */
    public function save($value): void
    {
        $this->store->set($value, $this->key);
    }
}