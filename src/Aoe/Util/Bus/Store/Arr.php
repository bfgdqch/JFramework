<?php
/**
 * Created by PhpStorm.
 * User: Qch
 * Date: 2016/9/10
 * Time: 12:01
 */

namespace Aoe\Util\Bus\Store;

use Aoe\Util\File;


/**
 * # 读写PHP数组文件
 *
 */
class Arr extends Folder
{
    public function get(string $key, $options = null): array | string
    {
        return File::getArray($key);
    }
}