<?php
/**
 * Created by PhpStorm.
 * User: Qch
 * Date: 2016/9/8
 * Time: 16:49
 */

namespace Aoe\Util\Bus\Store;

use Aoe\Util\Bus\Cache;
use Aoe\Util\File;
use Aoe\Util\Singleton;

/**
 * # 文件缓存 [文件名 => 文件内容]
 */
class Folder extends Cache
{
    use Singleton;

    /**
     * @param string $key
     * @param mixed  $options
     *   true: 数组
     *   null: 内容
     *   array: 模板
     *
     * @return array|string
     */
    public function get(string $key, $options = null): array | string
    {
        return File::get($key, $options);
    }
    
    public function set(mixed $value, string $key): int
    {
        return File::add($key, $value) ? 1 : 0;
    }
    
    public function has(int | string $key, bool | int $before = false): bool
    {
        return is_file($key) and (!$before or filemtime($key) > $before);
    }
    
    protected function do_delete(int | string $key): int
    {
        return File::remove($key) ? 1 : 0;
    }
    
    public function getExpire(int | string $key): false | int
    {
        return filemtime($key);
    }
}