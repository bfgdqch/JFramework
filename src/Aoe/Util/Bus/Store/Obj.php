<?php

namespace Aoe\Util\Bus\Store;


/**
 * # 读写Json文件
 */
class Obj extends Folder
{
    public function get(string $key, $options = null): array | string
    {
        $str = parent::get($key);
        
        if (!$str) return [];
        
        $r = json_decode($str, true);
        return is_array($r) ? $r : [];
    }
    
    public function set(mixed $value, string $key): int
    {
        $str = is_string($value) ? $value : self::json($value);
        
        return parent::set($str, $key);
    }
    
    public static function json(mixed $obj): string
    {
        return json_encode(
            $obj,
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK,
        );
    }
}