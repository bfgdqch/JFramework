<?php
/**
 * Created by PhpStorm.
 * User: Qch
 * Date: 2016/9/8
 * Time: 17:02
 */

namespace Aoe\Util\Bus;


/**
 * # 缓存
 */
abstract class Cache implements ICache
{
    public function delete(array | string $keys): int
    {
        if (is_array($keys)) {
            foreach ($keys as $key) $this->do_delete($key);
            return count($keys);
        }
        return $this->do_delete($keys);
    }
    
    /**
     * 删除单个元素
     *
     * @param string $key 元素索引
     *
     * @return int
     */
    abstract protected function do_delete(string $key): int;
    
    public function getExpire(string $key): false | int { return false; }
    
    public function clear() {}
}