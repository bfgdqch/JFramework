<?php

namespace Aoe\Util\Bus;


use Exception;

/**
 * # 持久化接口
 */
interface ISolid
{
    /**
     * ## 根据ID获取信息
     *
     * @param string $key
     * @param        $options
     *   true : include文件        --File
     *   true : 同步               --Synchronizer
     *   string[]|string: 数据列   --ORM
     *   string[]|string: 文件属性 --File
     *   array: 参数              --File
     *
     * @return mixed
     * @throws Exception
     */
    public function get(string $key, $options = null): mixed;
    
    /**
     * ## 设置
     *
     * @param        $value mixed 键值对/值
     * @param string $key   键
     *
     * @return int 影响的行数
     * @throws Exception
     */
    public function set(mixed $value, string $key): int;
    
    /**
     * ## 失效时间
     *
     * @param string    $key
     *
     * @return int|false 失效时间 false = 永不过期
     */
    public function getExpire(string $key): int | false;
    
    /**
     * ## 删除
     *
     * @param array|string $keys
     *
     * @return int 影响的行数
     * @throws Exception
     */
    public function delete(array | string $keys): int;
    
}