<?php

namespace Aoe\Util\Bus\Config;


/**
 * # 快捷使用级联配置
 *   * 获取：当前节点取不到数据，则级联上级获取。
 *   * 修改：修改当前配置层级的数据
 */
trait Configurable
{
    /**
     * @var Config 可级联的配置器
     */
    private(set) Config $config {
        get {
            return $this->config;
        }
    }

}