<?php


namespace Aoe\Util;


use Aoe\Util\Swap\Mixin;

/**
 * # 本地化
 */
class Language extends Mixin
{
    protected string $key = 'zh-cn';

    public function setLocale(string $key): void
    {
        $this->key = strtolower($key);
    }

    public function compile(string $dir): void
    {
        $file = join(DIRECTORY_SEPARATOR, [$dir, 'i18n', "$this->key.php"]);
        if (!is_file($file)) return;
        $this->load_value($this->key, fn() => File::getArray($file) ?: []);
    }
    
    public function translate(string $msg, array $context = []): string
    {
        return message($this->values[$msg] ?? $msg, $context);
    }
}