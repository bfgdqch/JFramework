<?php

namespace Aoe\Util\Provider;

use Closure;

interface MirrorIfc
{
    public function addDestructor(Closure $destructor): void;
}