<?php

namespace Aoe\Util\Provider;

use closure;

trait Mirror
{
    /**
     * @var closure[]
     */
    protected array $destructors = [];

    public function __destruct()
    {
        foreach ($this->destructors as $fun) $fun();
    }

    /**
     * @param Closure(): void $destructor
     * @noinspection PhpUnused
     */
    public function addDestructor(Closure $destructor): void
    {
        $this->destructors[] = $destructor;
    }
}