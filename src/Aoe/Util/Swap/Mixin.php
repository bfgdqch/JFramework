<?php


namespace Aoe\Util\Swap;

use Closure;

/**
 * # 混合配置类
 *   * 从多点读取数组，自动混合成一个数组
 *
 */
class Mixin
{
    /**
     * @var array 数据
     */
    protected array $values = [];
    /**
     * @var array<Closure> 加载器
     */
    protected array $loaders = [];
    
    /**
     * 加载数据
     *
     * @param string       $key
     * @param Closure|null $loader
     */
    protected function load_value(string $key, ?Closure $loader = null): void
    {
        if ($loader) $this->loaders[$key] = $loader;
        isset($this->loaders[$key]) && $this->_merge_value(($this->loaders[$key])());
    }
    
    /**
     * ## 注册条目
     *
     * @param array $values
     */
    private function _merge_value(array $values): void
    {
        $this->values = array_merge($this->values, $values);
    }
}