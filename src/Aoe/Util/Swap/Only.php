<?php

namespace Aoe\Util\Swap;

/**
 * # 虚拟只读类
 */
abstract class Only
{
    /**
     * All attributes set on the container.
     *
     * @var array
     */
    protected array $attributes = [];
    
    public function __get(string $key): mixed
    {
        $getter = 'get' . ucfirst($key);
        
        // 方法优先
        if (method_exists($this, $getter)) return $this->$getter();
        
        return $this->get($key);
    }
    
    public function get(?string $key = null, $default = null): mixed
    {
        if (null === $key) return $this->attributes;
        return $this->attributes[$key] ?? $default;
    }
    
    public function __isset(string $key): bool
    {
        return $this->has($key);
    }
    
    public function has(string $key): bool
    {
        return isset($this->attributes[$key]);
    }
    
    public function getKeys(): array
    {
        return array_keys($this->attributes);
    }
    
}