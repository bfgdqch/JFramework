<?php
/**
 * Created by PhpStorm.
 * User: DELL-PC
 * Date: 2018/8/2
 * Time: 12:12
 */

namespace Aoe\Emulator\Processor;

use Aoe\Database\SqlBuilder;
use Aoe\Emulator\Schema\Model;

/**
 * # 标记处理器
 *
 */
class Processor
{
    /**
     * @var string table
     */
    protected string $table;
    
    public function __construct(protected Model $model, protected SqlBuilder $builder)
    {
        $this->table = $model->getTableName();
    }
}