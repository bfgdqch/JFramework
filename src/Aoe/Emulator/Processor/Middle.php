<?php

namespace Aoe\Emulator\Processor;

use Throwable;

abstract class Middle extends Processor
{
    /**
     * @var int[]|int
     */
    protected array | int $id = 0;
    /**
     * 参与计算的元素
     *
     * @var ?string[]
     */
    protected ?array $elements = null;
    /**
     * @param int|int[] $id
     */
    public function setId(array | int $id): static
    {
        try {
            $this->id = $this->builder->checkIds($id);
        } catch (Throwable) {}
        
        return $this;
    }
}