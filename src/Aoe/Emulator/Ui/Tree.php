<?php

namespace Aoe\Emulator\Ui;

use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ui\Widget\Widget;
use Exception;
use Throwable;

class Tree extends Component
{
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function render(?array $declarer = null): array
    {
        $declarer = parent::render($declarer);
        
        $tree = [];
        
        if ($declarer[Key::ELEMENTS]) {
            $tree[Key::ELEMENTS] = Widget::renderWidgets($this->context, $declarer[Key::ELEMENTS]);
        }

        try {
            $tree['treeProps']['keys'][Key::LABEL] = $this->context->getModel()->getKey();
        }catch (Throwable) {}


        $tree[Component::API] = $this->pathinfo('load');
        $tree[Component::SUBMIT] = $declarer[self::SUBMIT] ?? $this->pathinfo('save');
        
        return $tree;
    }
}