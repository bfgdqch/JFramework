<?php


namespace Aoe\Emulator\Ui;


use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ui\Cell\Cell;
use Exception;
use JetBrains\PhpStorm\ArrayShape;

/**
 * ## Table
 *
 *  columns: Column[]
 *  url: Url 数据来源 返回 {rows, select, pagination}
 *  embedded: Button[] 行按钮
 *  pagination: object 分页
 *
 *  stripe:  boolean  是否为斑马纹
 *  border: boolean 是否带有纵向边框
 *  size:  medium/small/mini
 *  fit:  boolean 列的宽度是否自撑开
 *  show-header:  boolean  是否显示表头
 *  highlight-current-row:  boolean  是否要高亮当前行
 *
 */
class Table extends Component
{
    const string PAGINATION = 'pagination';

    private static array $copied = [self::PAGINATION, Component::API];
    
    /**
     * @throws Exception
     */
    #[ArrayShape([
        Key::ELEMENTS => "array",
        self::PAGINATION => "array",
        self::API        => "string",
    ])]
    function render(?array $declarer = null): array
    {
        if (null === $declarer) $declarer = $this->declarer;
        
        $table = $this->copy(self::$copied, $declarer);
        
        if (isset($declarer[Key::ELEMENTS]))
            $table[Key::ELEMENTS] = Cell::renderCells($this->context, $declarer[Key::ELEMENTS]);

        return $table;
    }
}