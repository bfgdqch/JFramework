<?php


namespace Aoe\Emulator\Ui;

class Component
{
    const string SUBMIT   = 'action';
    const string ACTION   = 'action';
    const string OPTIONS = 'options';

    const string URL      = 'url';
    /**
     * module/controller/action?p1/p2
     */
    const string API      = 'api';
    /**
     * VIEW_CONTROLLER 视图控制器名称
     */
    const string VC       = 'view';
    /**
     * API_CONTROLLER 增删改控制器名称
     */
    const string AC       = 'api';

    protected array $declarer {
        get {
            return $this->context->declarer;
        }
    }
    
    public function __construct(readonly public Context $context){}

    /**
     * 组件渲染接口
     *
     * @param array|null $declarer
     *
     * @return array 组件定义
     */
    public function render(?array $declarer = null): array
    {
        return $declarer ?? $this->declarer;
    }

    /**
     * 直接搬运过来
     *
     * @param string[] $keys
     * @param array $from
     * @param array $to
     * @return array
     */
    protected function copy(array $keys, array $from, array &$to = []): array
    {
        foreach ($keys as $k) if (isset($from[$k])) $to[$k] = $from[$k];
        return $to;
    }

    protected function pathinfo(?string $action = null, ?string $controller = null): string
    {
        return $this->context->pathinfo($action, $controller);
    }
}