<?php

namespace Aoe\Emulator\Ui;

use Aoe\Emulator\Controller\Aoe;
use Aoe\Emulator\Schema\Model;
use Aoe\Intent\Params;
use Aoe\Intent\Request\Http;
use Aoe\Intent\Request\Request;
use Exception;

class Context
{
    public array $declarer {
        get {
            $this->controller->getViewDeclarer($this->action);
        }
    }

    public ?string $action {
        get {
            return $this->request->section?->action;
        }
    }

    public function __construct(readonly public Aoe $controller, readonly public Request $request)
    {
    }

    public function pathinfo(?string $action = null, ?string $controller = null, ?Params $params= null): string
    {
        return ($this->request instanceof Http)
            ? $this->request->buildPathinfo($action, $controller, $params)
            : $this->request->section->forkPathinfo($action, $controller, $params);
    }

    public function getOptionsOf(?string $controller = null, ?Params $params= null): array
    {
        return [
            'pathinfo' => $this->pathinfo('options', $controller, $params),
        ];
    }

    /**
     * @throws Exception
     */
    public function getModel(?string $name = null): Model
    {
        return $this->controller->getModel($name);
    }
}