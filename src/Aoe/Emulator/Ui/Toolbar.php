<?php


namespace Aoe\Emulator\Ui;

/**
 * # Toolbar
 *
 *  'items': Button[]
 */
class Toolbar extends Component
{
    const string ITEMS = 'items';
    
    public function render(?array $declarer = null): array
    {
        if (null === $declarer) $declarer = $this->declarer;
        
        if (isset($declarer[self::ITEMS])) {
            $declarer[self::ITEMS] = $this->renderButtons($declarer[self::ITEMS]);
        }
        return $declarer;
    }
    
    public function renderButtons(array $items): array
    {
        $ts = [];
        foreach ($items as $item) $ts[] = new Component($this->context)->render($item);
        return $ts;
    }
}