<?php

namespace Aoe\Emulator\Ui\Cell;

use Aoe\Emulator\Schema\Element\Independent;
use Aoe\Emulator\Ui\Context;
use Aoe\Emulator\Ui\Item;

class Cell extends Item
{
    const string PIC       = 'pic';
    const string MARK      = 'mark';
    const string TAG       = 'tag';
    const string REMOTE    = 'remote';
    const string CELL      = 'cell';

    protected static array $cells = [
        self::PIC => Cell::class,
        self::MARK => Mark::class,
        self::TAG  => Mark::class,
        self::REMOTE => Remote::class,
    ];

    public static function renderCell(Context $context, Independent $element, array $declarer): array
    {
        $class = self::_get_cell_class($declarer);
        return new $class($context, $element)->render($declarer);
    }

    public static function renderCells(Context $context, ?array $declarers = null): array
    {
        return Item::renderItems($context, $declarers, Cell::RenderCell(...));
    }

    private static function _get_cell_class(array $declarer): string
    {
        if (!isset($declarer[self::CELL])) return CELL::class;
        return self::$cells[$declarer[self::CELL]] ?? CELL::class;
    }
}