<?php

namespace Aoe\Emulator\Ui\Cell;

use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ui\Component;

class Remote extends Cell
{
    public function render(?array $declarer = null): array
    {
        $declarer = parent::render($declarer);

        $declarer[Component::OPTIONS] = $this->pathinfo(Key::URL_OPTIONS, $this->element->name);

        return $declarer;
    }
}