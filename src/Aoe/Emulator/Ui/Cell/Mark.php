<?php

namespace Aoe\Emulator\Ui\Cell;

use Aoe\Emulator\Schema\Element\OptionsProvider;
use Aoe\Emulator\Ui\Component;

class Mark extends Cell
{
    public function render(?array $declarer = null): array
    {
        $declarer = parent::render($declarer);
        $declarer[Component::OPTIONS] = OptionsProvider::get($this->element)->labels;

        return $declarer;
    }
}