<?php

namespace Aoe\Emulator\Ui\Widget;

use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ui\Component;
use JetBrains\PhpStorm\ArrayShape;


class Attach extends Widget
{
    #[ArrayShape([
        'action' => 'string',
    ])]
    public function render(?array $declarer = null): array
    {
        $uploader = parent::render($declarer);
        array_not_set($uploader, Component::ACTION, $this->pathinfo(Key::URL_UPLOAD, $this->element->name));
        // $uploader['session']= $this->context->getSessionId();
        return $uploader;
    }
}