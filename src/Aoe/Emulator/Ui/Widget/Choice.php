<?php


namespace Aoe\Emulator\Ui\Widget;

use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ifc\Type;
use Aoe\Emulator\Ui\Component;

/**
 * # 选择域
 *
 *    border: boolean 显示边框
 */
class Choice extends Widget
{
    const string WHERE   = 'where';
    
    public function render(?array $declarer = null): array
    {
        $declarer = parent::render($declarer);
        if (isset($declarer[Component::OPTIONS])) return $declarer;

        $options = $this->element->get(Type::OPTIONS);
        $declarer[Component::OPTIONS] = $options ?? $this->pathinfo(Key::URL_OPTIONS, $this->element->name);
        
        return $declarer;
    }
//
//    /**
//     * @param array $schema
//     *
//     * @return array
//     * @throws Exception
//     */
//    private function _set_options(array $declarer): array
//    {
//        if ($this->element->isRelation()) return $this->_set_relation_options($schema);
//
//        // 没有选项的情况下
//        if (empty($this->element->get(KEY::OPTIONS))) return $schema;
//
//        // 使用预存
//        $schema[self::OPTIONS] = $this->element->get(KEY::OPTIONS);
//        return $schema;
//    }
//
//    /**
//     * @param array $schema
//     *
//     * @return array
//     * @throws Exception
//     */
//    private function _set_relation_options(array $schema = []): array
//    {
//        /** @var Relation $element */
//        $element = $this->element;
//        $model   = $element->getRelateModel();
//
//        $where = $schema[self::WHERE] ?? null;
//
//        $name = $model->name;
//        $api = self::AC;
//        if ($model->isTree()) {
//            $path = $this->request->pathinfo('tree', $name);
//            $schema['keys'] = [ 'value' => 'id', 'label' => $model->getKey() ];
//        } else {
//            $path = $this->request->pathinfo(Type::OPTIONS, $name);
//        }
//
//        if (is_string($where)) $path .= '/' . trim($where, '/');
//
//        $schema[self::OPTIONS] = $this->request->pattern("$api$path");
//
//        return $schema;
//    }
}