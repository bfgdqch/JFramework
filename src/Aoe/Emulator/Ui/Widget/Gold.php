<?php


namespace Aoe\Emulator\Ui\Widget;


use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ifc\Type;
use JetBrains\PhpStorm\ArrayShape;

/**
 * # 日期域
 *
 *  level: year/month/date
 *  editable: boolean
 *  format: 显示格式
 */
class Gold extends Widget
{
    #[ArrayShape([
        Key::NAME  => "string",
        Key::LABEL => "string",
    ])]
    public function render(?array $declarer = null): array
    {
        $declarer = parent::render($declarer);
        
        $level  = $declarer[Type::MODE] ?? $this->element->get(Type::MODE);
        $format = match ($level) {
            Type::MONTH_MODE => 'YYYY-MM',
            Type::DAY_MODE  => 'YYYY-MM-DD',
            default => 'YYYY-MM-DD HH:mm:ss'
        };
        $mode = match($level) {
            Type::MONTH_MODE => 'month',
            default => 'date'
        };
        
        $declarer['mode']         = $mode;
        $declarer['format']       = $format;
        $declarer['valueType']    = $format;
        
        if ($level === Type::TIME_MODE) $declarer['enableTimePicker'] = true;
        
        return $declarer;
    }
}