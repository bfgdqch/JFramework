<?php

namespace Aoe\Emulator\Ui\Widget;


use Aoe\Emulator\Ifc\Key;
use JetBrains\PhpStorm\ArrayShape;

/**
 * # 时间域
 */
class Silver extends Widget
{
    #[ArrayShape([
        Key::NAME  => "string",
        Key::LABEL => "string",
    ])]
    public function render(?array $declarer = null): array
    {
        $declarer                 = parent::render($declarer);
        $declarer['format']       = 'HH:mm:ss';
        $declarer['value-format'] = 'HH:mm:ss';
        
        return $declarer;
    }
}