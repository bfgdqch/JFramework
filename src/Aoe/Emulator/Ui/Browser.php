<?php


namespace Aoe\Emulator\Ui;

use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ifc\Vu;
use Aoe\Emulator\Ui\Widget\Widget;
use JetBrains\PhpStorm\ArrayShape;

/**
 * ## CURD
 *
 *    'search':
 *    'toolbar':
 */
class Browser extends Table
{
    const string SEARCH    = 'search';   // 查询 { Field[] }
    const string ADVANCE   = 'advance';  // 高级查询 { Field[] }
    const string EMBEDMENT = 'tools';    // 行内按钮 { Button[] }
    const string SIMPLE    = 'simple';   // 简单表格{ bool }
    const string SELECTOR  = 'selector'; // 行首选框 { bool }
    const string TREE      = 'tree';     // 左侧栏 { Tree }

    private static array $copied = [
        'dialogWidth',
    ];
    
    #[ArrayShape([
        self::EMBEDMENT  => "array",
        self::SEARCH     => "array",
        Vu::TOOLBAR      => "array",
        Key::ELEMENTS    => "array",
        self::PAGINATION => "array",
        self::SELECTOR   => "boolean",
        Component::API   => "string",
    ])]
    public function render(?array $declarer = null): array
    {
        $declarer = $declarer ?? $this->declarer;

        $curd = parent::render($declarer);

        $curd = $this->copy(self::$copied, $declarer, $curd);
        if (isset($declarer[self::SIMPLE]) && $declarer[self::SIMPLE]) return $curd;
        
        $tree = $this->makeTree($declarer);
        if ($tree) $curd[self::TREE] = $tree;
        
        $curd[self::SELECTOR] = $declarer[self::SELECTOR] ?? true;
        
        $embedment             = $declarer[self::EMBEDMENT] ?? $this->createDefaultEmbedment();
        $curd[self::EMBEDMENT] = new Toolbar($this->context)->renderButtons($embedment);
        
        $toolbar           = $declarer[Vu::TOOLBAR] ?? $this->createDefaultToolbar();
        $curd[Vu::TOOLBAR] = new Toolbar($this->context)->renderButtons($toolbar);
        
        if (isset($declarer[self::SEARCH]))
            $curd[self::SEARCH] = Widget::renderWidgets($this->context, $declarer[self::SEARCH]);
        
        if (isset($declarer[self::ADVANCE]))
            $curd[self::ADVANCE] = Widget::renderWidgets($this->context, $declarer[self::ADVANCE]);
        
        return $curd;
    }
    
    private function makeTree(array $schema): array | false
    {
        
        if (!isset($schema[self::TREE])) return false;
        $name = $schema[self::TREE];

        return $this->pathinfo('load', $name);
//        try {
//            $relation = $this->controller->getModel($name);
//            if (!$relation->isTree()) return false;
//            $r = $this->context->getTreeOptions($name);
//            if ($r->error) return false;
//
//            $key = $relation->getKey();
//            $tree = ['keys' => ['query' => $name, 'label' => $key, 'value' => 'id' ]];
//            $tree[Type::OPTIONS] = $r->getValue();
//            $tree['idType'] = $relation->isUuid() ? 'string' : 'number';
//
//            return $tree;
//        } catch (Throwable) {
//        }
//
//        return false;
    }
    
    /**
     * @return array<array{
     *     label: string,
     *     name: string,
     *     command: array{ url?: string, payload?: string, directive?: string },
     *     icon?: string,
     *     theme?: string }>
     */
    private function createDefaultEmbedment(): array
    {
        return [
            [
                'label'   => '修改',
                'name'    => 'edit',
                'icon'    => 'edit',
                'theme'   => 'success',
                'command' => [
                    'url'       => $this->context->pathinfo('edit'),
                    'payload'   => 'id',
                    'directive' => 'popup',
                ],
            ],
            [
                'label'   => '删除',
                'name'    => 'remove',
                'icon'    => 'delete',
                'theme'   => 'danger',
                'command' => [
                    'url'     => $this->context->pathinfo('remove'),
                    'payload' => 'id',
                ],
            ],
        ];
        
    }
    
    private function createDefaultToolbar(): array
    {
        return [
            [
                'label'   => '新建',
                'name'    => 'add',
                'icon'    => 'add',
                'theme'   => 'primary',
                'command' => [
                    'url'       => $this->pathinfo('add'),
                    'directive' => 'popup',
                ],
            ],
            [
                'label'   => '删除',
                'name'    => 'remove',
                'icon'    => 'delete',
                'theme'   => 'danger',
                'command' => [
                    'url'     => $this->pathinfo('remove'),
                    'payload' => 'id',
                ],
            ],
        ];
    }
    
}