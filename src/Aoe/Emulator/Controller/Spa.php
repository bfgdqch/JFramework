<?php


namespace Aoe\Emulator\Controller;

use Aoe\Intent\Render\Json;

/**
 * # 单页面数据渲染器
 */
class Spa extends Json
{
    protected function send_content(): void
    {
        $this->sendJson(
            [
                'type'    => 'json',
                'data'    => $this->response->getValue(),
                'message' => $this->response->message,
                'error'   => $this->response->error,
            ],
        );
    }
}