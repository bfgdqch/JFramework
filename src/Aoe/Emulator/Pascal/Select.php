<?php


namespace Aoe\Emulator\Pascal;

use Aoe\Emulator\Schema\Element\OptionsProvider;
use PDO;

class Select extends Pascal
{
    protected function bind($op = null): int
    {
        return PDO::PARAM_INT;
    }
    
    public function afterLoad($value): int
    {
        return convert_id($value) ? $value : 0;
    }
    
    public function display($value): mixed
    {
        $e = OptionsProvider::get($this->element);
        return $e? $e->getLabel($value) : parent::display($value);
    }
    
    /**
     * 处理将label值返回的情况
     */
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        // 转换
        if (!is_numeric($value)) {
            $e = OptionsProvider::get($this->element);
            if ($e) $value = $e->getValue($value);
        }
        return convert_int($value);
    }
    
    protected function _calculators(): array
    {
        return [];
    }
   
    protected function compares(): array
    {
        return $this->simple_compares();
    }
}