<?php

namespace Aoe\Emulator\Pascal;

use Aoe\Emulator\Schema\Element\Independent;
use Aoe\Util\Provider\MirrorIfc;
use Aoe\Util\Provider\Provider;

/**
 * @extends Provider<Independent, Pascal>
 */
class PascalProvider extends Provider
{
    /**
     * @param Independent $element
     * @return Pascal
     */
    static protected function create_object(MirrorIfc $element): Pascal
    {
        return Pascal::getInstance($element);
    }
}