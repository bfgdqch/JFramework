<?php

namespace Aoe\Emulator\Pascal;

use Aoe\Database\Operator;
use Aoe\Util\IdentityCard;

class Card extends Pascal
{
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        if ($operator === Operator::CARD_BIRTH_GREAT || $operator === Operator::CARD_BIRTH_LESS) {
            $value = (int)$value;
            return true;
        }

        $value = (string)$value;
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    protected function valid_rules(mixed $value): bool
    {
        return is_string($value) && IdentityCard::isValid($value) && parent::valid_rules($value);
    }
    
    protected function _calculators(): array
    {
        return [];
    }
    
    protected function compares(): array
    {
        return [
            '=',
            '<>',
            Operator::LIKE_OPERATOR,
            Operator::NOT_LIKE_OPERATOR,
            Operator::IN_OPERATOR,
            Operator::NOT_IN_OPERATOR,
            Operator::NOT_NULL_OPERATOR,
            Operator::IS_NULL_OPERATOR,
            Operator::CARD_BIRTH_GREAT,
            Operator::CARD_BIRTH_LESS,
        ];
    }
}