<?php

namespace Aoe\Emulator\Pascal;

use Aoe\Emulator\Ifc\Type;
use Aoe\Database\Operator;
use Aoe\Util\DateTool;
use PDO;

/**
 * # 年月/年月日
 */
class Second extends Pascal
{
    public function afterLoad($value): string
    {
        return DateTool::int2str((int)$value, $this->getFormat());
    }
    
    protected function getFormat(): string
    {
        $level = $this->element->get(Type::MODE);
        
        return match ($level) {
            Type::MONTH_MODE => DateTool::F_MONTH,
            Type::DAY_MODE => DateTool::F_DAY,
            default => DateTool::F_TIME
        };
    }
    
    protected function bind($op = null): int
    {
        return PDO::PARAM_INT;
    }
    
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        if (is_string($value)) $value = DateTool::str2int($value, $this->getFormat());
        return is_int($value);
    }
    
    protected function _calculators(): array
    {
        return [Operator::NOW_OPERATOR];
    }
    
    protected function compares(): array
    {
        return $this->math_compares();
    }
}