<?php

namespace Aoe\Emulator\Pascal;

use Aoe\Database\Operator;
use PDO;
use function convert_bool;

/**
 * # 布尔
 */
class Boolean extends Pascal
{
    protected function bind($op = null): int
    {
        return PDO::PARAM_BOOL;
    }
    
    public function afterLoad($value): bool
    {
        return !!$value;
    }
    
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        return convert_bool($value);
    }
    
    protected function _calculators(): array
    {
        return ['!'];
    }
    
    protected function compares(): array
    {
        return ['=', '<>', Operator::NOT_NULL_OPERATOR, Operator::IS_NULL_OPERATOR];
    }
}