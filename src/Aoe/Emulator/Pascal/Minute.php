<?php

namespace Aoe\Emulator\Pascal;

use PDO;
use Throwable;

/**
 * 时间 XX:XX:XX
 */
class Minute extends Pascal
{
    public function afterLoad($value): string
    {
        return substr(
            chunk_split(substr("000000$value", -1, 6), 2, ':'),
            0,
            6
        );
    }
    
    protected function bind($op = null): int
    {
        return PDO::PARAM_INT;
    }
    
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        try {
            $value = (int)(strtr($value, ':', ''));
            return 0 < $value && $value <= 240000;
        }catch (Throwable) {
            return false;
        }
    }
    
    protected function _calculators(): array
    {
        return [];
    }
    
    protected function compares(): array
    {
        return $this->math_compares();
    }
}