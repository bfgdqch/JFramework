<?php


namespace Aoe\Emulator\Pascal;

use PDO;
use function array2bit;
use function bit2array;

class Multi extends Pascal
{
    protected function bind(): int
    {
        return PDO::PARAM_INT;
    }
    
    public function afterLoad($value): array
    {
        if (empty($value)) return [];
        return bit2array($value);
    }
    
    protected function before_input($value): int | null
    {
        if (empty($value)) return 0;
        if (is_array($value)) $value = array2bit($value);
        else $value = 1 << ($value - 1);
        return $value;
    }
    
    protected function default_compare(): string
    {
        return '&';
    }
    
    protected function valid_core(mixed &$value, ?string $operator = null, bool $calculate = true): bool
    {
        return convert_int($value);
    }
    
    protected function _calculators(): array
    {
        return ['=', '|'/* 增加 */, '!&'/* 删除 */];
    }
    
    protected function compares(): array
    {
        return ['=', '&='/* 同时包含 */, '&'/* 包含 */];
    }
}