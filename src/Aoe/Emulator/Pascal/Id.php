<?php

namespace Aoe\Emulator\Pascal;


use Aoe\Database\Operator;
use PDO;

class Id extends Pascal
{
    protected function bind(): int
    {
        return PDO::PARAM_INT;
    }
    
    public function afterLoad($value): int
    {
        return convert_id($value) ? $value : 0;
    }
    
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        return convert_id($value);
    }
    
    protected function _calculators(): array
    {
        return [];
    }
    
    protected function compares(): array
    {
        return [
            '=',
            '<>',
            Operator::IN_OPERATOR,
            Operator::NOT_IN_OPERATOR,
        ];
    }
}