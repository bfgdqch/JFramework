<?php

namespace Aoe\Emulator\Pascal;

use Aoe\Database\Operator;
use Exception;

class Uuid extends Pascal
{
    protected function _calculators(): array
    {
        return [Operator::SET_NULL_OPERATOR];
    }
    
    protected function compares(): array
    {
        return [...$this->simple_compares(), Operator::CONTENT_OPERATOR];
    }
    
    /**
     * 级联选择结果是个数组
     *
     * @throws Exception
     */
    protected function valid_core(&$value, string $operator = '=', bool $calculate = true): bool
    {
        if ($calculate && $operator === '=' && empty($value)) {
            $value = null;
            return true;
        }
        // 数组的特殊情况
        if ($calculate && $operator === '=' && is_array($value)) $value = array_pop($value);

        $value = (string)$value;
        return true;
    }
}