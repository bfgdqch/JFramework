<?php


namespace Aoe\Emulator\Pascal;


use Aoe\Database\Operator;
use PDO;

class Pid extends Pascal
{
    protected function bind(): int
    {
        return PDO::PARAM_INT;
    }
    
    public function afterLoad($value): int
    {
        return convert_id($value) ? $value : 0;
    }
    
    protected function _calculators(): array
    {
        return [Operator::SET_NULL_OPERATOR];
    }
    
    protected function compares(): array
    {
        return [
            '=',
            '<>',
            Operator::IN_OPERATOR,
            Operator::NOT_IN_OPERATOR,
            Operator::NOT_NULL_OPERATOR,
            Operator::IS_NULL_OPERATOR,
            Operator::RELATION_OPERATOR,
        ];
    }
    
    protected function valid_core(&$value, string $operator = '=', bool $calculate = true): true
    {
        if ($calculate && $operator === '=' && empty($value)) {
            $value = null;
            return true;
        }
        
        // 级联选择结果是个数组
        if ($calculate && $operator === '=' && is_array($value)) $value = array_pop($value);
        if (!convert_int($value)) $value = null;
        return true;
    }
}