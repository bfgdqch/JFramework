<?php

namespace Aoe\Emulator\Pascal;


use Aoe\Database\Operator;

class Str extends Pascal
{
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        $value = (string)$value;
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    protected function valid_rules(mixed $value): bool
    {
        return $this->_len_valid($value) && parent::valid_rules($value);
    }

    private function _len_valid(string $str): bool
    {
        if ($this->element->has(self::LEN))
            return  strlen($str) === intval($this->element->get(self::LEN));

        if ($this->element->has(self::MIN) && strlen($str) < intval($this->element->get(self::MIN)))
            return false;

        return !$this->element->has(self::MAX) || strlen($str) <= intval($this->element->get(self::MAX));
    }
    
    protected function _calculators(): array
    {
        return ['.', Operator::REPLACE_OPERATOR];
    }
    
    protected function compares(): array
    {
        return [
            '=',
            '<>',
            Operator::LIKE_OPERATOR,
            Operator::NOT_LIKE_OPERATOR,
            Operator::IN_OPERATOR,
            Operator::NOT_IN_OPERATOR,
            Operator::NOT_NULL_OPERATOR,
            Operator::IS_NULL_OPERATOR,
            Operator::CONTENT_OPERATOR,
        ];
    }
}