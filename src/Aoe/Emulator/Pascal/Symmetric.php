<?php

namespace Aoe\Emulator\Pascal;

use Aoe\Database\Operator;

class Symmetric extends Pascal
{
    public function save($value): string
    {
        //$key previously generated safely, ie: openssl_random_pseudo_bytes
        //$plaintext = "message to be encrypted";
        // $cipher="AES-128-CBC";
        //$iv_len = openssl_cipher_iv_length($cipher);
        //$iv = openssl_random_pseudo_bytes($iv_len);
        //$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        //$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        //$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        return $value;
    }

    public function afterLoad($value): string
    {
        // $c = base64_decode($ciphertext);
        // $iv_len = openssl_cipher_iv_length($cipher="AES-128-CBC");
        // $iv = substr($c, 0, $iv_len);
        // $hmac = substr($c, $iv_len, $sha2len=32);
        // $ciphertext_raw = substr($c, $iv_len+$sha2len);
        // $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        // $calc_mac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        // if (hash_equals($hmac, $calc_mac))// timing attack safe comparison
        // {
        //      echo $original_plaintext."\n";
        // }
        return $value;
    }
    protected function valid_core(&$value, string $operator = '=', bool $calculate = true): bool
    {
        $value = (string)$value;
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function _calculators(): array
    {
        return ['='];
    }

    /**
     * @inheritDoc
     */
    protected function compares(): array
    {
        return ['=', '<>', Operator::IS_NULL_OPERATOR, Operator::CONTENT_OPERATOR];
    }
}