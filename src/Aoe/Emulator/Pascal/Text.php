<?php

namespace Aoe\Emulator\Pascal;

use Aoe\Database\Operator;

class Text extends Pascal
{
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        $value = (string)$value;
        return true;
    }
    
    protected function _calculators(): array
    {
        return ['.', Operator::REPLACE_OPERATOR];
    }
    
    protected function compares(): array
    {
        return [
            Operator::NOT_NULL_OPERATOR,
            Operator::IS_NULL_OPERATOR,
            Operator::LIKE_OPERATOR,
            Operator::NOT_LIKE_OPERATOR,
        ];
    }
}