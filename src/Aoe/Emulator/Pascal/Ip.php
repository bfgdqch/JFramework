<?php

namespace Aoe\Emulator\Pascal;


use Aoe\Database\Operator;
use PDO;

class Ip extends Pascal
{
    protected function bind(): int
    {
        return PDO::PARAM_INT;
    }
    
    public function afterLoad($value): string
    {
        return long2ip($value);
    }
    
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        if (is_string($value)) {
            if ($value === '::1' || strtolower($value) === 'localhost') $value = '127.0.0.1';
            $value = ip2long($value);
        }

        return is_int($value);
    }
    
    protected function _calculators(): array
    {
        return [Operator::IP_OPERATOR];
    }
    
    protected function compares(): array
    {
        return [
            '=',
            '<>',
            Operator::IN_OPERATOR,
            Operator::NOT_IN_OPERATOR,
            Operator::NOT_NULL_OPERATOR,
            Operator::IS_NULL_OPERATOR,
        ];
    }
}