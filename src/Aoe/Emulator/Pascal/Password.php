<?php

namespace Aoe\Emulator\Pascal;

class Password extends Pascal
{
    protected function valid_core(mixed &$value, string $operator = '=', bool $calculate = true): bool
    {
        $value = (string)$value;
        return true;
    }

    protected function _calculators(): array
    {
        return [];
    }

    protected function compares(): array
    {
        return [];
    }

    public function save(mixed $value): string
    {
        return password_hash($value, PASSWORD_DEFAULT);
    }
}