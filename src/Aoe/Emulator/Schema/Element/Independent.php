<?php


namespace Aoe\Emulator\Schema\Element;


use Aoe\Emulator\Ifc\Key;
use Aoe\Emulator\Ifc\Type;
use Aoe\Emulator\Schema\Micro;

/**
 * # 独立元素
 */
class Independent extends Micro
{
    public function getPascal(): string
    {
        return $this->get(Key::PASCAL, Type::STRING);
    }
    
    public function getColumn(): string
    {
        return $this->get(Key::COLUMN, $this->name);
    }
    
    public function isRelation(): bool
    {
        return false;
    }
    
    public function canNull(): bool
    {
        return !$this->get(Type::NULLABLE, false);
    }
    
    public function hasDefault(): bool
    {
        return $this->has(Type::DFT);
    }
    
    public function getDefault(): mixed
    {
        return $this->get(Type::DFT);
    }
    
    public function must(): bool
    {
        return (bool)$this->get(Type::MUST, false);
    }

    public function isVirtual(): bool
    {
        return (bool)$this->get(KEY::VIRTUAL, false);
    }

}