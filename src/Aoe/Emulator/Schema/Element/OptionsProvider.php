<?php

namespace Aoe\Emulator\Schema\Element;

use Aoe\Emulator\Ifc\Key;
use Aoe\Util\Provider\MirrorIfc;
use Aoe\Util\Provider\Provider;

/**
 * @extends Provider<Independent, Options>
 */
class OptionsProvider extends Provider
{
    /**
     * @param Independent $element
     * @return Options
     */
    protected static function create_object(MirrorIfc $element): Options
    {
        return new Options($element->get(Key::OPTIONS));
    }
}