<?php


namespace Aoe\Emulator\Schema\Element;


use Aoe\Emulator\Schema\Model;

/**
 * # 元素
 */
class Element extends Independent
{
    public function __construct(
        array           $def,
        string          $name,
        protected Model $model
    ) {
        parent::__construct($def, $name);
    }
}