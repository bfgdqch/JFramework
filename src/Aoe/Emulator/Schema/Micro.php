<?php


namespace Aoe\Emulator\Schema;

use Aoe\Emulator\Ifc\Key;
use Aoe\Util\Provider\Mirror;
use Aoe\Util\Provider\MirrorIfc;
use Aoe\Util\Swap\Rom;

/**
 * # 元件 支持功能插座
 */
class Micro extends Rom implements MirrorIfc
{
    use Mirror;

    /**
     * @var string 标签
     */
    public string $label {
        get {
            return $this->get(Key::LABEL, $this->name);
        }
    }
    public function __construct(
        array            $def,
        /**
         * @var string 名称
         */
        protected(set) string $name {
            get {
                return $this->name;
            }
        }
    ) {
        parent::__construct($def);
    }
}