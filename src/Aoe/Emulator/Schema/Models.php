<?php


namespace Aoe\Emulator\Schema;


use Aoe\Attributes\Container\Dependent;
use Aoe\Emulator\Smart;
use Aoe\Util\Manager\NamedCollection;

/**
 * # 模型组
 *
 * @extends NamedCollection<Model>
 */
class Models extends NamedCollection
{
    public function __construct(array $defs, #[Dependent] protected(set) ?Smart $adapter {
        get {
            return $this->adapter;
        }
    }, protected(set) string $name {
        get {
            return $this->name;
        }
    })
    {
        parent::__construct($defs);
    }

    protected function create_object($name): Model
    {
        return new Model($this->named_items->get(lcfirst($name), []), $name, $this);
    }
}