<?php

namespace Aoe\Emulator\Ifc;

class Vu
{
    const string TOOLBAR  = 'toolbar';
    const string TREE     = 'tree';
    const string TABLE    = 'table';
    const string FORM     = 'form';

    /**
     * 组件名
     */
    const string COMPONENT = 'component';
    /**
     * 组件参数
     */
    const string SCHEMA    = 'schema';
    /**
     * 样式
     */
    const string STYLE     = 'style';
}
