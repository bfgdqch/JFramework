<?php

namespace Aoe\Emulator\Ifc;

/**
 * # 常用键
 *
 *   * '#', '&', '$', '@' => 展示, 原始, 未定义, 未定义
 */
class Key
{
    /**
     * 标识符
     */
    const string NAME  = 'name';
    /**
     * 标题
     */
    const string LABEL = 'label';
    /**
     * 值
     */
    const string VALUE = 'value';
    /**
     * 数据库列名称
     */
    const string COLUMN = 'column';
    /**
     * 元素类型
     */
    const string PASCAL = 'pascal';
    /**
     * 运算符
     */
    const string OPERATOR = 'operator';
    /**
     * 关联模型名
     */
    const string RELATION = 'relation';
    /**
     * 子元素
     */
    const string ELEMENTS = 'elements';
    /**
     * 虚拟字段
     */
    const string VIRTUAL = 'virtual';

    const string OPTIONS = 'options';

    const string URL_OPTIONS = 'options';
    const string URL_UPLOAD  = 'upload';

    const string API_INSERT = 'insert';
    const string VIEW_ADD   = 'add';
    const string API_UPDATE = 'update';
    const string VIEW_EDIT  = 'edit';
}