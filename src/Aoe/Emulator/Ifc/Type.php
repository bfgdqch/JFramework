<?php

namespace Aoe\Emulator\Ifc;

class Type
{
    /**
     * UUID
     */
    const string UUID     = 'uuid';
    /**
     * ID(内置)
     */
    const string ID       = 'id';
    /**
     * 状态(内置)
     */
    const string STATUS   = '__status__';
    /**
     * PID(内置)
     */
    const string PID      = '__pid__';
    /**
     * 所有者字段(内置)
     */
    const string OWNER    = '__owner__';
    
    /**
     * 创建时间(内置)
     */
    const string CREATE     = '__create_time__';
    /**
     * 修改时间(内置)
     */
    const string UPDATE     = '__update_time__';
    
    /**
     * 布尔
     */
    const string BOOL     = "bool";
    /**
     * 整数 { length: [1, 2, 4, 8] }
     */
    const string INTEGER  = "int";
    /**
     * 关联 { model: string }
     */
    const string RELATION = 'relation';
    /**
     * char { length: <255 }
     */
    const string STRING   = "str";
    /**
     * 密码
     */
    const string PASSWORD = 'password';
    /**
     * 身份证
     */
    const string CARD     = 'card';
    /**
     * 手机
     */
    const string PHONE    = 'phone';
    /**
     * 电子邮件
     */
    const string MAIL     = 'mail';
    /**
     * 文本 *char { length: int; }
     */
    const string TEXT     = "text";
    /**
     * 年月日时
     */
    const string DATETIME = 'datetime';
    /**
     * 年月日 { mode: 'month' | 'day' }
     */
    const string DATE     = "date";
    /**
     * 时间
     */
    const string TIME     = "time";
    /**
     * 单选 { options: [] }
     */
    const string SELECT   = "select";
    /**
     * 多选标记项
     */
    const string MARK     = 'mark';
    /**
     * 浮点数 { double: boolean }
     */
    const string DECIMAL  = "decimal";
    /**
     * 文件
     */
    const string FILE     = "file";
    /**
     * 图片
     */
    const string IMG      = 'img';
    /**
     * IP地址
     */
    const string IP       = "ip";
    
    
    /**
     * 无符号{bool}: int
     */
    const string UNSIGNED = 'unsigned';
    /**
     * 长度{int}: [int: 字节数, char: 字符串长度]
     */
    const string LENGTH   = 'length';
    /**
     * 唯一 { bool }: all
     */
    const string UNIQUE   = 'unique';
    /**
     * 可为空{bool}: all
     */
    const string NULLABLE = 'full';
    /**
     * 选项{Opt}: select
     */
    const string OPTIONS  = 'options';
    /**
     * 必填{bool}: all
     */
    const string MUST     = 'must';
    /**
     * 默认值{mixed}: all
     */
    const string DFT      = 'default';
    /**
     * 自增{bool}: all
     */
    const string AUTO_INC = 'auto';
    /**
     * 双精度{bool}: decimal
     */
    const string DOUBLE   = 'double';
    /**
     * 验证定义<Rule[]>: all
     */
    const string RULES    = 'rules';
    /**
     * 多选 { bool }: select
     */
    const string MULTI    = 'multiple';
    /**
     * 日期等级
     */
    const string MODE     = 'mode';
    
    /**
     * 月
     */
    const string MONTH_MODE = 'month';
    /**
     * 日
     */
    const string DAY_MODE   = 'date';
    /**
     * 全时间
     */
    const string TIME_MODE  = 'datetime';

    public static function defaultDefinition(string $name): bool | array
    {
        return match ($name) {
            Type::STATUS => [
                Key::NAME        => Type::STATUS,
                Key::PASCAL      => Type::BOOL,
                Type::NULLABLE => false,
                Key::LABEL       => '状态',
                Type::OPTIONS  => ['禁用', '启用'],
                Type::DFT      => 1,
            ],
            Type::ID => [
                Key::NAME        => Type::ID,
                Key::PASCAL      => Type::ID,
                Type::MUST     => true,
                Type::NULLABLE => false,
                Type::UNIQUE   => true,
                Type::AUTO_INC => true,
            ],
            Type::PID => [
                Key::NAME   => Type::PID,
                Key::PASCAL => Type::ID,
            ],
            Type::OWNER => [
                Key::NAME          => Type::OWNER,
                Key::PASCAL        => Type::INTEGER,
                Type::NULLABLE => false,
                Key::LABEL         => '所有者',
                Type::DFT      => 0,
            ],
            Type::CREATE => [
                Key::NAME          => Type::CREATE,
                Key::PASCAL        => Type::INTEGER,
                Key::LABEL         => '创建时间',
                Type::DFT      => 0,
            ],
            Type::UPDATE => [
                Key::NAME          => Type::UPDATE,
                Key::PASCAL        => Type::INTEGER,
                Key::LABEL         => '修改时间',
                Type::DFT      => 0,
            ],
            default => false,
        };
    }
}