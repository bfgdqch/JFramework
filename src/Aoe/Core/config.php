<?php

use Aoe\Intent\Router;

/**
 * # 系统默认配置
 *    * 数组只支持标量数据
 *    * 内核启动前加载的类需要在这里定义，启动后的可以在应用配置中定义
 */
return [
    // 服务定义
    'service'   => [
        'Language'     => '\\Aoe\\Util\\Language',
        'Logger'       => '\\Aoe\\Util\\Logger\\Logger',
        'Router'       => '\\Aoe\\Intent\\Router',
        'Http'         => '\\Aoe\\Intent\\Request\\Http',
        'Html'         => '\\Aoe\\Intent\\Response\\Render\\Html',
        'Json'         => '\\Aoe\\Intent\\Response\\Render\\Json',
        'Manager'      => '\\Aoe\\Util\\Manager\\ClosureCollection',
        'Template'     => '\\Aoe\\Util\\Template',
        'Database'     => '\\Aoe\\Provider\\Database',
        'Sync'         => '\\Aoe\\Util\\Bus\\Convertor',
        'Smart'        => '\\Aoe\\Emulator\\Smart',
    ],
    
    // 路由定义
    'Router'    => [
        // 路由元定义
        Router::ENTITIES  => [
            'module'     => '[A-Za-z]+',
            'controller' => '[A-Za-z]+',
            'action'     => '[A-Za-z][A-Za-z_]+',
            'pairs'      => '[/A-Za-z0-9]+',
            'command'    => '[A-Za-z]+',
        ],
        // 路由规则定义
        Router::RULES     => [
            'login' => ['module' => 'auth', 'controller' => 'admin', 'action' => 'index'],
            ':module/:controller/:action/:pairs' => [],
            ':module/:controller/:action'        => [],
            ':module/:controller'                => ['action' => 'Index'],
            ''                                   => ['module' => 'Index', 'controller' => 'Index', 'action' => 'Index'],
        ],
        // 自定义路由元解析方法
        Router::CALLBACKS => ['pairs' => 'vars_form_path'],
    ],
];