<?php
/**
 * Created by PhpStorm.
 * User: DELL-PC
 * Date: 2018/8/23
 * Time: 11:14
 */

namespace Aoe\Core;


use Aoe\Attributes\Container\Dependent;


/**
 * # 模型有机组合成项目
 */
class Common extends Module
{
    public function __construct(#[Dependent] Kernel $kernel, string $name)
    {
        $name            = ucfirst($name);
        $this->dir       = __APP_DIR__ . DIRECTORY_SEPARATOR . $name;
        $this->namespace = __APP_NS__ . '\\' . $name;
        
        parent::__construct($kernel, $name);
    }
}