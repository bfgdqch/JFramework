<?php
/**
 * Created by PhpStorm.
 * User: Qch
 * Date: 2016/8/13
 * Time: 17:04
 *
 * 内核入口文件
 */

use Aoe\Core\Kernel;

const DS = DIRECTORY_SEPARATOR;

/**
 * 运行环境
 */
defined('__ENV__') or define('__ENV__', 'DEBUG');

/**
 * 错误处理
 */
__ENV__ === 'DEBUG' and error_reporting(E_ALL);

/**
 * 环境系统常量
 */
define('IS_WIN', str_contains(PHP_OS, 'WIN'));
/**
 * 开启跨域
 */
defined('OPEN_CORS') or define('OPEN_CORS', false);

/**
 * 默认命名空间
 */
defined('__APP_NS__') or define('__APP_NS__', 'Application');

/**
 * 默认命名空间对应的模块目录
 */
defined('__APP_DIR__') or define('__APP_DIR__', __RUNTIME__ . DS . 'src'. DS . __APP_NS__);
/**
 * 通用视图存放目录
 */
defined('__VIEW__') or define('__VIEW__', __APP_DIR__ . '/views');
/**
 * 数据库目录
 */
defined('__DATA_DIR__') or define('__DATA_DIR__', __RUNTIME__ . DS . 'database');
/**
 * 临时文件存放目录
 */
defined('__TEMP__') or define('__TEMP__', __WWW_DIR__ . DS . 'temp');
/**
 * 上传文件保存目录, 方便下载，需要在public目录下
 */
defined('__UPLOAD_DIR__') or define('__UPLOAD_DIR__', __WWW_DIR__ . DS . 'files');

/**
 * 上传文件的下载地址
 */
defined('__DOWNLOAD_URL__') or define('__DOWNLOAD_URL__', '/files');
/**
 * 静态文件路径
 */
defined('__STATICS_URL__') or define('__STATICS_URL__', '/statics');

/**
 * 锁定
 */
defined('LOCK') or define('LOCK', __WWW_DIR__ . DS . '.lock');

/**
 * 首页
 */
defined('HOME_CMD') or define('HOME_CMD', '/index.php/index/index/index');

/**
 * 登录界面
 */
defined('LOGIN_CMD') or define('LOGIN_CMD', 'auth/admin/index');
/**
 * 用户信息获取通道
 */
defined('USER_CMD') or define('USER_CMD', 'auth/admin/info');
/**
 * 权限添加通道
 */
defined('AUTH_CMD') or define('AUTH_CMD', 'auth/admin/pass');

/**
 * 安装通道
 */
defined('INSTALL_CMD') or define('INSTALL_CMD', 'index/index/install');

/**
 * 默认配置文件名
 */
defined('CONFIG_JSON') or define('CONFIG_JSON', 'config');

/**
 * 数据库表名前缀
 */
defined('TABLE_PREFIX') or define('TABLE_PREFIX', 'aoe_');
/**
 * 依赖存放路径 * 当前使用composer，不再需要vendor目录
 */
// defined('__VENDOR__') or define('__VENDOR__', __RUNTIME__ . DIRECTORY_SEPARATOR . 'vendor');


/**
 * 通用函数
 */
include_once("function.php");

// 导入加载器
//include_once("Loader.php");
//$loader = new Loader;
//$loader->register();

// 注册框架前缀
// $loader->addNamespace('J', dirname(__LIB_DIR__));

new Kernel()->start();
