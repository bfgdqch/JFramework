<?php

namespace Aoe\Intent\Render;

class Stdout extends Render
{
    protected function send(): void
    {
        echo $this->response->error ? $this->response->message : $this->response->getValue();
    }
}