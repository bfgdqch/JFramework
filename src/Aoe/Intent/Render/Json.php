<?php


namespace Aoe\Intent\Render;

/**
 * # Json响应处理器
 */
class Json extends Hypertext
{
    protected function send_exception(): void
    {
        $exception = $this->response->getValue();
        $this->sendJson(
            [
                'message' => $exception?->getMessage(),
                'file'    => $exception?->getFile(),
                'line'    => $exception?->getLine(),
                'error'   => true,
            ],
        );
    }
    
    protected function sendJson(array $value): void
    {
        $records = $this->response->getRecords();
        if ($records) $value['trace'] = $records;

        $this->send_header('Content-type', 'application/json');
        echo json_encode($value);
    }
    
    protected function send_redirect(): void {}
    
    protected function send_content(): void
    {
        $value = $this->response->getValue();
        if (!is_array($value)) return;
        $this->sendJson($value);
    }
}