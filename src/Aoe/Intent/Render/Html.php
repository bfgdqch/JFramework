<?php


namespace Aoe\Intent\Render;

use Aoe\Util\Logger\Record;

/**
 * # HTML响应处理器
 */
class Html extends Hypertext
{
    protected function send_exception(): void
    {
        $e = $this->response->getValue();
        $trace = $this->response->getRecords();

        include __VIEW__ . DIRECTORY_SEPARATOR . 'exception.php';
    }
    
    protected function send_content(): void
    {
        echo $this->response->getValue();
        $this->_trace_echo();
    }
    
    protected function send_redirect(): void
    {
        $url = $this->response->getValue();
        if ($this->response) {
            include __VIEW__ . DIRECTORY_SEPARATOR . 'jump.php';
        } else {
            $this->send_header('Location', $url);
        }
    }
    
    private function _trace_echo(): void
    {
        $records = $this->response->getRecords();
        if (!$records) return;
        
        echo '<div class="framework-trace-box">';
        foreach ($records as $record)
            echo '<code class="framework-trace-line">' . $this->_log_echo($record) . '</code><br>';
        echo '</div>';
    }
    
    private function _log_echo(Record $record): string
    {
        return sprintf(
            "[%'05s]%'*20s#%'-15s@%s",
            $record->line,
            $record->file,
            $record->name,
            $record->message,
        );
    }
}