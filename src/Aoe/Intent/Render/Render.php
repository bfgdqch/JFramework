<?php

namespace Aoe\Intent\Render;

use Aoe\Intent\Response;

/**
 * # 响应结果渲染器
 */
abstract class Render
{
    const string HTML = 'Html';
    const string JSON = 'Json';
    const string STD  = 'Std';

    protected static array $renders = [
        self::HTML     => Html::class,
        self::JSON     => Json::class,
        self::STD      => Stdout::class,
    ];

    public static function print(Response $response): void
    {
        self::_get_render_instance($response)->send();
    }

    protected function __construct(protected Response $response){}

    abstract protected function send(): void;

    private static function _get_render_instance(Response $response): static
    {
        $type = $response->render;
        $class = self::$renders[$type] ?? $type;
        if (!class_exists($class)) { $class = Stdout::class; }

        return new $class($response);
    }
}