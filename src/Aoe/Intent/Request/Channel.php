<?php


namespace Aoe\Intent\Request;


use Aoe\Attributes\Container\Dependent;
use Aoe\Core\Kernel;

/**
 * # 虚拟命令行
 */
class Channel extends Request
{
    /** @noinspection PhpGetterAndSetterCanBeReplacedWithPropertyHooksInspection */
    public function __construct(#[Dependent] Kernel $kernel, protected string $pathinfo, array $params = [])
    {
        parent::__construct($kernel);
        $this->params->append($params);
    }
    
    public function getPathinfo(): string
    {
        return $this->pathinfo;
    }
    
    public function fit(array $factor): bool
    {
        return true;
    }
    
    public function getMethod(): string
    {
        return 'command';
    }
}