<?php

namespace Aoe\Intent\User;

/**
 * # 权限对象

 */
class Power
{
    /**
     * 权限-整数-新增
     */
    const int ADD     = 0x1;
    /**
     * 权限-整数-读取
     */
    const int READ    = 0x22;
    /**
     * 权限-整数-修改
     */
    const int EDIT    = 0x44;
    /**
     * 权限-整数-删除
     */
    const int DEL     = 0x88;
    /**
     * 权限-整数-管理员
     */
    const int ADMIN   = 0xf0f0f0f0;
    /**
     * 权限-整数-特殊
     */
    const int SPACIAL = 0xffff0000;
}