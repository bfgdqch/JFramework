<?php

namespace Aoe\Intent;

use Aoe\Util\Swap\Rom;

/**
 * # 请求参数
 */
class Params extends Rom
{
    public function encodeQueryString(?array $keys = null): string
    {
        if (empty($keys)) $keys = $this->getKeys();

        $str = [''];
        foreach ($keys as $k) {
            $v = $this->get($k);
            if (!is_scalar($v)) continue;

            $str[] = $k;
            $str[] = urlencode($v);
        }
        return join('/', $str);
    }

    public function append(array ...$arrays): void
    {
        $this->attributes = array_merge_recursive($this->attributes, ...$arrays);
    }

    public bool $empty {
        get => empty($this->attributes);
    }
}