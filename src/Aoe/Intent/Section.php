<?php

namespace Aoe\Intent;

use Aoe\Util\Swap\Rom;

/**
 * # 请求信息，一般是路由解析结果
 *
 * @property ?string action
 * @property ?string controller
 * @property ?string module
 * @property ?Params pairs 请求参数(解析)
 * @property ?string method 请求的方法
 * @property ?string ext 扩展名
 * @property ?string regex 路由正则
 */
class Section extends Rom
{
    /**
     * @var string 模块/控制器/方法
     */
    public string $short {
        get => "$this->module/$this->controller/$this->action";
    }
    
    public string $resource {
        get => "$this->module::$this->controller";
    }

    /**
     * 模块/[控制器]/[方法]
     */
    public function forkShort(?string $action = null, ?string $controller = null): string
    {
        $controller = $controller ?? $this->controller;
        $action     = $action ?? $this->action;

        return "/$this->module/$controller/$action";
    }
    /**
     * [模块]/[控制器]/[方法]
     */
    public static function buildShort(string $action, string $controller, string $module): string
    {
        return "/$module/$controller/$action";
    }

    /**
     * 制作一个PATHINFO
     *    * params 必须设置才能参与运算
     *
     * @param string|null $action
     * @param string|null $controller
     * @param Params|null $params 不自动使用宿主的params
     * @param string|null $module
     * @return string
     */
    public function forkPathinfo(
        ?string $action = null,
        ?string $controller = null,
        ?Params $params = null,
        ?string $module = null,
    ): string
    {
        $params = $params === null ? $this->pairs : $params;
        return new self([
            "action" => $action ?? $this->action,
            "controller" => $controller ?? $this->controller,
            "pairs" => $params,
            "module" => $module ?? $this->module,
        ])->buildPathinfo();
    }

    public function buildPathinfo(): string
    {
        $query = ($this->pairs->empty ? '' : '/') . $this->pairs->encodeQueryString();
        return "$this->short$query";
    }
}