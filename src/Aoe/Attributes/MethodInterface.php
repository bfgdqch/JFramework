<?php

namespace Aoe\Attributes;


use ReflectionMethod;

/**
 * # 方法注解接口
 */
interface MethodInterface
{
    /**
     * @param ReflectionMethod $rm
     * @param                  $obj
     *
     * @return mixed
     */
    public function forMethod(ReflectionMethod $rm, $obj): mixed;
}