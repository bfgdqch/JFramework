<?php


namespace Aoe\Attributes\Container;

use Aoe\Attributes\ParameterInterface;
use Aoe\Core\Kernel;
use Attribute;
use ReflectionParameter;

/**
 * # 配置注入
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class Argument implements ParameterInterface
{
    /**
     * Config constructor.
     *
     * @param ?string $key
     */
    public function __construct(protected ?string $key = null){}
    
    public function forParameters(Kernel $kernel, ReflectionParameter $parameter, array &$args): bool
    {
        $args[] = $kernel->config->get($this->key ?? $parameter->getDeclaringClass()->getShortName(), []);
        return true;
    }
}