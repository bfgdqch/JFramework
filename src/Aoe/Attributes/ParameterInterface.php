<?php


namespace Aoe\Attributes;


use Aoe\Core\Kernel;
use ReflectionParameter;

/**
 * # 参数注解接口
 */
interface ParameterInterface
{
    /**
     * @param Kernel              $kernel
     * @param ReflectionParameter $parameter
     * @param array               $args
     *
     * @return bool 参数已填充
     */
    public function forParameters(Kernel $kernel, ReflectionParameter $parameter, array &$args): bool;
}