<?php


namespace Aoe\Attributes;


use Aoe\Core\Kernel;

/**
 * # 类注解接口
 */
interface ClassInterface
{
    public function forClass(Kernel $kernel, $obj): void;
}