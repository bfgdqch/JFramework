<?php


namespace Aoe\Attributes\Controller;

use Aoe\Core\Controller;
use Aoe\Intent\Request\Request;
use Attribute;
use Closure;

/**
 * # 配置响应处理器
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_CLASS)]
class SetResponse extends CtrlAttribute
{
    const string RESPONSE_INJECT = '配置${type}响应处理器';
    
    /**
     * Response constructor.
     *
     * @param string $type
     */
    public function __construct(protected string $type) {}
    
    protected function core(Request $request, Controller $controller, ?Closure $next): void
    {
        $controller->recorder->Log(self::RESPONSE_INJECT, ['type' => $this->type]);
        $request->response->render = $this->type;
        $next && $next($request);
    }
}