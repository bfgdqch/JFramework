<?php

namespace Aoe\Attributes\Controller;

use Aoe\Attributes\ClassInterface;
use Aoe\Attributes\MethodInterface;
use Aoe\Core\Controller;
use Aoe\Core\Kernel;
use Aoe\Intent\Request\Request;
use Attribute;
use Closure;
use ReflectionMethod;

/**
 * # 控制器(及其方法)中间件注解工具
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_CLASS)]
abstract class CtrlAttribute implements MethodInterface, ClassInterface
{
    use Middleware;
    
    /**
     * @param ReflectionMethod|null $rm
     * @param Controller            $obj
     *
     * @return Closure(closure(Request $request): void $next): void
     */
    public function forMethod(?ReflectionMethod $rm, $obj): Closure
    {
        return $this->inject($obj);
    }
    
    /**
     * @param Kernel     $kernel
     * @param Controller $obj
     *
     * @return void
     */
    public function forClass(Kernel $kernel, $obj): void
    {
        $obj->setMiddleware($this->inject($obj));
    }
}