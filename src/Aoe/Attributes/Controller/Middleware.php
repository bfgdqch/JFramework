<?php

namespace Aoe\Attributes\Controller;

use Aoe\Core\Controller;
use Aoe\Intent\Request\Request;
use Closure;

/**
 * # 中间件注入
 */
trait Middleware
{
    /**
     * @param Controller $controller
     *
     * @return Closure(Closure(Request $request): void): void
     */
    protected function inject(Controller $controller): Closure
    {
        return fn (?Closure $next) => fn (Request $intent) => $this->core($intent, $controller, $next);
    }
    
    /**
     * 执行切面操作
     *
     * @param Request    $request
     * @param Controller $controller
     * @param null | Closure(Request $request): void    $next
     *
     * @return void
     */
    abstract protected function core(Request $request, Controller $controller, ?Closure $next): void;
}