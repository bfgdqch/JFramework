<?php

namespace Aoe\Attributes\Controller;


use Aoe\Core\Controller;
use Aoe\Intent\Request\Request;
use Aoe\Util\Logger\Logger;
use Attribute;
use Closure;

/**
 * # 输出控制台信息
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_CLASS)]
class Trace extends CtrlAttribute
{
    const string TRACE = '日志开启';

    public function __construct(protected int $level = Logger::ERROR) {}
    
    protected function core(Request $request, Controller $controller, ?Closure $next): void
    {
        $next && $next($request);
        $recorder = $controller->recorder;
        $recorder->Log(self::TRACE);
        $request->response->openTrace($recorder, $this->level);
    }
}