<?php

namespace Aoe\Attributes\Controller;

use Aoe\Core\Controller;
use Aoe\Intent\Request\Http;
use Aoe\Intent\Request\Request;
use Attribute;
use Closure;

/**
 * # 必须使用 Http 请求
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_CLASS)]
class HttpMust extends CtrlAttribute
{
    const string HTTP_ONLY = '只能从Http访问';
    
    /**
     * @inheritDoc
     */
    protected function core(Request $request, Controller $controller, ?Closure $next): void
    {
        if ($request instanceof Http) {
            $next && $next($request);
            return;
        }

        $controller->recorder->Error(self::HTTP_ONLY);
        $request->response->error(self::HTTP_ONLY);
    }
}