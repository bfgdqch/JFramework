<!DOCTYPE html>
<html lang="zh">
<head>
    <title>发生了错误</title>
</head>
<style>
    table {
        width:100%;
    }
    .level1 {
        color: red;
    }
    .level2 {
        color: green;
    }
    .e-file {
        width: 200px;
    }
    .e-line {
        width: 50px;
    }
    .e-line--num, .t-line--num {
        text-align: right;
        padding-right: 10px;
    }
    .e-message--text {
        padding-left: 30px;
    }
    .t-name {
        width: 100px;
    }
    .t-name--text {
        text-align: center;
    }
</style>
<body>
<?php if (isset($e)) { ?>
    <h1>
        <?php echo $e->getMessage(); ?>
        <span>
            发生在文件<?php echo $e->getFile() ?>的第<?php echo $e->getLine() ?>行
        </span>
    </h1>
    <table>
        <caption>堆栈信息</caption>
        <thead><tr>
            <th class="e-file">文件</th>
            <th class="e-line">行号</th>
            <th>方法</th>
        </tr>></thead>
        <tbody>
            <?php foreach ($e->getTrace() as $tc) { ?><tr>
                <td>
                    <?php echo  basename($tc['file']) ?>
                </td>
                <td class="e-line--num">
                    <?php echo $tc['line'] ?>
                </td>
                <td class="e-message--text">
                    <?php echo $tc['function'] ?>
                </td>
            </tr><?php } ?>
        </tbody>
    </table>
<?php } ?>
<br />
<?php
if (isset($trace) && is_array($trace)) { ?>
    <table>
        <caption>反馈信息</caption>
        <thead><tr>
        <th class="e-file">文件</th>
        <th class="e-line">行号</th>
        <th class="t-name">名称</th>
        <th>信息</th>
        </tr>></thead>
        <tbody>
        <?php foreach ($trace as $t) {?><tr>
            <td>
                <?php echo basename($t->file) ?>
            </td>
            <td class="t-line--num">
                <?php echo $t->line ?>
            </td>
            <td class="t-name--text">
                <?php echo $t->name ?>
            </td>
            <td>
                <?php if ($t->level === 1) { ?>
                    <span class="level1"><?php echo $t->message; ?></span>
                <?php } else { ?>
                    <span class="level2"><?php echo $t->message; ?></span>
                <?php } ?>
            </td>
        </tr><?php } ?>
        </tbody>
    </table>
<?php } ?>
</body>
</html>