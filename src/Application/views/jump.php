<!DOCTYPE HTML>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>错误跳转</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link href="<?php
    echo __STATICS_URL__; ?>/public/statics/404.css" rel="stylesheet" type="text/css"/>
    <meta http-equiv="refresh" content="5;url=<?php
    if (!empty($url)) {
        echo $url;
    } else {
        echo '/';
    } ?>">
    <!-- 设置5秒返回  -->
    <script type="text/javascript">
        setTimeout(
            function () {
                top.location =;
            },
            5000
        )
    </script>
</head>
<body>
<div class="error404">
    <div class="info">
        <h1>404</h1>
        <h2>抱歉，您访问的页面不存在或已被删除！ (｡•ˇ‸ˇ•｡)</h2>
        <p class="p1">5秒钟后将带您返回首页</p>
        
        <a href="/" class="btn">返回首页</a>
        <a href="#" class="btn btn-brown">返回上一步</a>
    </div>
</div>
</body>
</html>