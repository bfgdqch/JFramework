<?php

namespace Application\Index\Controller;


use Aoe\Core\Controller;
use Aoe\Intent\Request\Http;
use Aoe\Intent\Request\Request;
use Exception;

/**
 * # 首页
 * @noinspection PhpUnused
 */
class Index extends Controller
{
    const string ERR_DATA_DIR       = '数据目录创建失败';
    const string ERR_DATA_FILE      = '数据文件创建失败';
    const string ERR_INSTALL_LOCKED = '安装已锁定';
    const string ERR_LOCKFILE       = '安装锁定文件创建失败';

    /**
     * ## 安装页面
     * @noinspection PhpUnused
     */
    protected function install_action(Request $request): void
    {
        if (is_locked()) {
            $request->response->error(self::ERR_INSTALL_LOCKED);
            return;
        }

        // 安装数据库
        $name = __DATA_DIR__ . DIRECTORY_SEPARATOR . 'data.sqlite';
        $dir = dirname($name);
        // 无法创建数据库目录
        if (!is_dir($dir) && !mkdir($dir)) {
            $request->response->error(self::ERR_DATA_DIR);
            return;
        }
        // 创建数据库文件
        $file = fopen($name, "w");
        // 无法写入数据库文件
        if (!$file) {
            $request->response->error(self::ERR_DATA_FILE);
            return;
        }
        fclose($file);

        // 写入默认配置
        $this->kernel->config->save([
            'database' => [
                [
                    'type' => 'sqlite',
                    'name' => 'data.sqlite',
                ],
            ],
            'app' => 'AOE通用数据中台',
            'company' => '鸿瑞科技',
            'soft' => '数据中台',
            'version' => '3.1.0',
        ]);

        // 写锁文件
        $file = fopen(LOCK, "w"); // 打开文件，如果文件不存在则创建
        if ($file) {
            fwrite($file, date('Y-m-d')); // 写入内容
            fclose($file); // 关闭文件
            $request->response->complete();
            return;
        }
        $request->response->error(self::ERR_LOCKFILE);
    }
    
    /**
     * ## 首页
     *
     * @param Http $http
     *
     * @throws Exception
     *
     * @noinspection PhpUnused
     */
    protected function index_action(Http $http): void
    {
        $http->response->setRedirect('/index.html');
        // $http->response->complete('Hello ' . rand(1, 200));
        // $http->getResponse()->setRedirect('/index.html');
        // $http->complete('<h1>你好</h1><div><a href="index.php/index/index/template">页面解析</a></div>');
        // $console = f_open('php://stdout', 'a');
        // f_write($console, json_encode($_REQUEST) . PHP_EOL);
    }
}