<?php

namespace Application\System\Controller;

use Aoe\Attributes\Controller\HttpMust;
use Aoe\Attributes\Controller\SetResponse;
use Aoe\Attributes\Controller\Super;
use Aoe\Emulator\Controller\Aoe;
use Aoe\Emulator\Controller\Spa;
use Aoe\Emulator\Ifc\Vu;
use Aoe\Intent\Request\Http;
use Aoe\Intent\Request\Request;


#[Super]
#[SetResponse(Spa::class)]
class Settings extends Aoe
{
    /**
     * # 显示系统配置界面
     *
     * @param Http $http
     *
     * @return void
     * @noinspection PhpUnused
     */
    #[HttpMust]
    protected function index_action(Http $http): void
    {
        $definition             = $this->get_action_def('edit') ?? [];
        $schema                 = $definition[Vu::SCHEMA] ?? [];
        $schema['action']       = $http->url->makeUrl('aoe/settings/save');
        $schema['api']          = $http->url->makeUrl('aoe/settings/load');
        $schema['json']         = true;
        $definition[Vu::SCHEMA] = $schema;
        $http->response->complete($definition);
    }

    /** @noinspection PhpUnused */
    protected function save_action(Request $intent): void
    {
        $config = $intent->getParam('value');
        if ($config) $this->kernel->config->save(json_decode($config, true));
        
        $intent->response->message = '配置完成';
        $intent->response->ok('配置完成');
    }

    /** @noinspection PhpUnused */
    protected function load_action(Request $intent): void
    {
        $intent->response->complete(
            $this->kernel->config->all(),
        );
    }
}