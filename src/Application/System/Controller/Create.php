<?php

namespace Application\System\Controller;

use Aoe\Attributes\Controller\HttpMust;
use Aoe\Attributes\Controller\SetResponse;
use Aoe\Attributes\Controller\Super;
use Aoe\Attributes\Controller\Trace;
use Aoe\Core\Controller;
use Aoe\Emulator\Controller\Spa;
use Aoe\Emulator\Ifc\Key;
use Aoe\Intent\Request\Http;
use Aoe\Intent\Request\Request;
use Throwable;

/**
 * # 模块管理
 * @noinspection PhpUnused
 */
#[Trace]
#[Super]
#[SetResponse(Spa::class)]
class Create extends Controller
{
    /**
     * ## 模块管理页
     * @noinspection PhpUnused
     */
    #[HttpMust]
    protected function index_action(Http $http): void
    {
        $http->response->complete(
            [
                'component' => 'schema',
                'schema'    => [
                    'submit' => $http->url->makeUrl('aoe/create/insert'),
                    'url'    => $http->url->makeUrl('aoe/create/get'),
                ],
                'style'  => ['width' => '100%'],
            ],
        );
    }
    
    /**
     * @noinspection PhpUnused
     */
    protected function get_action(Request $request): void
    {
        $name = $request->getParam('module');
        if (empty($name)) {
            $request->response->error('模块名不能为空');
            return;
        }
        try {
            $request->response->complete($this->kernel->getModule($name)->config->all());
        } catch (Throwable) {
            $request->response->error('模块获取失败');
        }
    }

    /** @noinspection PhpUnused */
    protected function insert_action(Request $request): void
    {
        $value = json_decode($request->getParam('module'), true);
        
        if (empty($value[Key::NAME])) {
            $request->response->error('模块名不能为空');
            return;
        }
        
        $name = filter_char($value[Key::NAME]);
        
        $value[Key::NAME] = $name;
        
        try {
            $kernel = $this->kernel;
            
            // 创建module目录
            $module = $kernel->getModule($name);
            
            $dir = $module->dir;
            if (!is_dir($dir)) mkdir($dir, 0700, true);
            
            $config = $module->config;
            
            // 保存module配置
            $config->save($value);
            
            $rebuild = $request->getParam('database');
            convert_bool($rebuild);
            
            if (!$rebuild) {
                $request->response->ok('配置已保存');
                return;
            }
            
            $r = $request->channel("aoe/install/$name");
            $request->response->complete($r->getValue(), $r->error, $r->message);
            
        } catch (Throwable $e) {
            $request->response->error($e->getMessage());
        }
    }

    /** @noinspection PhpUnused */
    protected function remove_action(Request $request): void
    {
        $name = $request->getParam('module');
        if (empty($name)) {
            $request->response->error('模块名不能为空');
            return;
        }
        $name = filter_char($name);
        try {
            $r = $request->channel("aoe/install/$name/uninstall/1");
            $request->response->complete($r->getValue(), $r->error, $r->message);
        } catch (Throwable $e) {
            $request->response->error($e->getMessage());
        }
    }
}